---
hide:
  - toc
author: à compléter
title: Liste des challenges
---

# Web - serveur


!!! abstract "Description générale de la catégorie"

    Le Web représente l'ensemble des liaisons entre les différents équipements électroniques et informatiques qui communiquent. Le versant &laquo;serveur&raquo; correspond à l'hébergement des ressources qui sont demandées lors d'une requête HTTP (fichiers HTML, bases de données, etc.). Lors  d'attaques informatiques, ce sont généralement les serveur qui sont ciblés 
    afin d'en récupérer les données.

    Dans cette catégorie, on propose divers challenges permettant d'interagir avec des serveurs admettant diverses failles de 
    sécurité. Ces challenges ont pour but de sensibiliser aux bonnes pratiques sur la sécurisation des serveurs Web.

    
!!! warning "Autres challenges"

    D'autres challenges dans différentes catégories sont également disponibles sur le <a href='https://cybersecurite.forge.apps.education.fr/cyber/3.Challenges/presentation/' target='_blank'>site principal</a> dédié à la cybersécurité.

    Etes-vous prêt à relever les défis ?


<hr style="height:5px;color:red;background-color:red;">

!!! note "Répertoires (challenge issu de la plateforme <a href='https://www.challenges-kids.fr/index.php' target='_blank'>challenges-kids.fr</a>)"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐ 1 flag

        **Objectif :** comprendre une arborescence de fichiers

    ![](../ressources_challenges_web_serveur/Challenges_externes/repertoires.png){: .center }
    
    Un message secret est caché dans un répertoire...

    Saurez-vous faire preuve de perspicacité pour le découvrir ?

    <a href='https://www.challenges-kids.fr/index.php' target='_blank'>Challenges-kids.fr</a> est une plateforme ouverte à la contribution et d'aide à l'enseignement du Hacking pour les petits et les grands, qui offre de nombreux challenges de difficultés graduées pour progresser.

    [_Accéder au challenge_](https://www.challenges-kids.fr/categories/web/chall3/){ .md-button target="_blank" rel="noopener" }


!!! note "SQL injection - Authentification (challenge issu de la plateforme <a href='https://www.root-me.org/' target='_blank'>Root Me</a>)"

    !!! tip "Présentation"

        **Niveau :** NSI

        **Difficulté :** ⭐⭐⭐ 1 flag

        **Objectif :** comprendre ce qu'est une attaque par injection SQL et les dangers qu'elle représente

    ![](../ressources_challenges_web_serveur/Challenges_externes/injection_sql_auth.png){: .center }
    
    Serez-vous capable de pénétrer dans une base de données pour vous authentifier à la place de l'administrateur et 
    récupérer ainsi son flag ?

    <a href='https://www.root-me.org/' target='_blank'>Root Me</a> est une plateforme de CTF française très connue et très qualitative qui offre de nombreux challenges dans diverses catégories, ainsi que de nombreuses ressources.

    [_Accéder au challenge_](https://www.root-me.org/fr/Challenges/Web-Serveur/SQL-injection-Authentification){ .md-button target="_blank" rel="noopener" }



!!! note "SQL injection - String (challenge issu de la plateforme <a href='https://www.root-me.org/' target='_blank'>Root Me</a>)"

    !!! tip "Présentation"

        **Niveau :** NSI

        **Difficulté :** ⭐⭐⭐ 1 flag

        **Objectif :** comprendre ce qu'est une attaque par injection SQL et les dangers qu'elle représente
    
    ![](../ressources_challenges_web_serveur/Challenges_externes/injection_sql_string.png){: .center }
    
    Une base de données, plusieurs tables...
    
    Arriverez-vous à récupérer le flag de l'administrateur ?

    <a href='https://www.root-me.org/' target='_blank'>Root Me</a> est une plateforme de CTF française très connue et très qualitative qui offre de nombreux challenges dans diverses catégories, ainsi que de nombreuses ressources.

    [_Accéder au challenge_](https://www.root-me.org/fr/Challenges/Web-Serveur/SQL-injection-String){ .md-button target="_blank" rel="noopener" }




!!! note "URL ? (challenge issu de la plateforme <a href='https://www.challenges-kids.fr/index.php' target='_blank'>challenges-kids.fr</a>)"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐⭐ 1 flag

        **Objectif :** comprendre et manipuler les URL

    ![](../ressources_challenges_web_serveur/Challenges_externes/fee_clochette.png){: .center }
    
    La fée clochette n'est pas autorisée à recevoir le flag.
    
    Ni l'utilisateur ni le mot de passe ne sont bons.

    Saurez-vous trouver le bon utilisateur ?

    <a href='https://www.challenges-kids.fr/index.php' target='_blank'>Challenges-kids.fr</a> est une plateforme ouverte à la contribution et d'aide à l'enseignement du Hacking pour les petits et les grands, qui offre de nombreux challenges de difficultés graduées pour progresser.

    [_Accéder au challenge_](https://www.challenges-kids.fr/categories/web/chall4/){ .md-button target="_blank" rel="noopener" }


!!! note "Tetris (challenge issu de la plateforme <a href='https://www.challenges-kids.fr/index.php' target='_blank'>challenges-kids.fr</a>)"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐ 1 flag

        **Objectif :** survivre...

    ![](../ressources_challenges_web_serveur/Challenges_externes/tetris.png){: .center }
    
    Devenez le meilleur à Tetris et exploser le score !!!

    <a href='https://www.challenges-kids.fr/index.php' target='_blank'>Challenges-kids.fr</a> est une plateforme ouverte à la contribution et d'aide à l'enseignement du Hacking pour les petits et les grands, qui offre de nombreux challenges de difficultés graduées pour progresser.

    [_Accéder au challenge_](https://www.challenges-kids.fr/categories/web/chall8/){ .md-button target="_blank" rel="noopener" }